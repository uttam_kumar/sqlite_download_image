package com.uttam.sqliteupdownimage;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.Toast;

public class SQLiteDatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "image_database.db";
    private static final String TABLE_NAME = "image_table";
    private static final String IMAGE_DATA = "image_data";
    private static final String IMAGE_ID = "id";
    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;


    //change every time when onUpgrade method is called
    private static final int VERSION_CODE = 201;
    private static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" + IMAGE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"  + IMAGE_DATA + " blob);";

    private static final String SELECT_ALL_DATA = "SELECT * FROM " + TABLE_NAME;

    private Context mContext;
    ProgressDialog progressDialog;

    public SQLiteDatabaseHelper(Context mContext) {
        super(mContext, DATABASE_NAME, null, VERSION_CODE);
        this.mContext = mContext;
        progressDialog = new ProgressDialog(mContext);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try {
            //Toast.makeText(context,"onCreate database is called",Toast.LENGTH_SHORT).show();
            sqLiteDatabase.execSQL(CREATE_TABLE);

        } catch (Exception e) {
            //Toast.makeText(context,"Exception for creating : "+e,Toast.LENGTH_SHORT).show();
            //e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        try {
            //Toast.makeText(context,"onUpgrade is called",Toast.LENGTH_SHORT).show();
            sqLiteDatabase.execSQL(DROP_TABLE);
            onCreate(sqLiteDatabase);
        } catch (Exception e) {
            //Toast.makeText(context,"Exception : "+e,Toast.LENGTH_SHORT).show();
        }
    }

    //insert data into table
    public void insertImage(byte[] im_data) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(IMAGE_DATA, im_data);

        if(rowCountFromTable()>0){
            sqLiteDatabase.update(TABLE_NAME, cv, "id=1", null);

            Toast.makeText(mContext,"updated successfully",Toast.LENGTH_SHORT).show();
        }else{
            //if row successfully then return a row id else return -1
            long rowId = sqLiteDatabase.insert(TABLE_NAME, null, cv);
            if(rowId!=-1){
                Toast.makeText(mContext,"inserted successfully",Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(mContext,"error",Toast.LENGTH_SHORT).show();
            }
        }


    }

    //display All data from table
    public byte[] displayImage() {
        byte[] blob=null;
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(SELECT_ALL_DATA, null);
        if (cursor.getCount() == 0) {
            Toast.makeText(mContext,"checkUrlFromTable is result--------> "+cursor.getCount(),Toast.LENGTH_SHORT).show();
        } else {
            while (cursor.moveToNext()) {
                String c_id = cursor.getString(0).trim();
                blob = cursor.getBlob(1);

                Log.d("TAG", "displayAllData: cid: "+c_id+" blob: "+blob);

            }
        }
        return blob;
    }



    //number of row of table
    public int rowCountFromTable(){
        SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
        Cursor cursor=sqLiteDatabase.rawQuery("SELECT * FROM "+TABLE_NAME,null);
        return cursor.getCount();
    }

}