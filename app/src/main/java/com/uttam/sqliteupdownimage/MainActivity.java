package com.uttam.sqliteupdownimage;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView profileImage,imageView;
    private Button saveBtn,displayBtn;
    private EditText imageName;
    String nametxt;
    private static final int PHOTO_GALLERY_PERMISSION=123;
    Uri filePath;
    Bitmap bitmap;
    byte[] byteArray,byteArray2;
    ByteArrayOutputStream stream;
    CheckRuntimePermission checkRuntimePermission;
    SQLiteDatabaseHelper sqLiteDatabaseHelper;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkRuntimePermission=new CheckRuntimePermission(MainActivity.this,MainActivity.this);
        sqLiteDatabaseHelper=new SQLiteDatabaseHelper(MainActivity.this);

        profileImage=findViewById(R.id.imageId);
        saveBtn=findViewById(R.id.saveBtnId);
        imageName=findViewById(R.id.editTextId);
        displayBtn=findViewById(R.id.displayBtnId);
        imageView=findViewById(R.id.image2Id);

        profileImage.setOnClickListener(this);
        saveBtn.setOnClickListener(this);

        displayBtn.setOnClickListener(this);
        progressDialog=new ProgressDialog(this);
    }



    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode==PHOTO_GALLERY_PERMISSION&&resultCode==RESULT_OK && data!=null){
            filePath=data.getData();
            try{
                InputStream inputStream=getContentResolver().openInputStream(filePath);
                bitmap=BitmapFactory.decodeStream(inputStream);
                profileImage.setImageBitmap(bitmap);

                try {
                    stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byteArray = stream.toByteArray();
                }catch (Exception e){
                    e.printStackTrace();
                }

            }catch (Exception e){
                Toast.makeText(this,"file not found",Toast.LENGTH_SHORT).show();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imageId:
                checkRuntimePermission.checkPermission();
                if(checkRuntimePermission.isPermissionNotGranted()){
                    Intent intent=new Intent(Intent.ACTION_PICK);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent,"Select Image"),PHOTO_GALLERY_PERMISSION);
                }else{
                    Log.d("TAG", "onClick: luiuito");
                }
                break;

            case R.id.saveBtnId:
                if(byteArray!=null){
                    progressDialog.setMessage("storing....");
                    progressDialog.show();
                    sqLiteDatabaseHelper.insertImage(byteArray);
                    progressDialog.dismiss();
                }
                break;

            case R.id.displayBtnId:
                byteArray2=sqLiteDatabaseHelper.displayImage();
                Log.d("TAG", "onClick: rowCountFromTable: "+byteArray2);
                if(byteArray2!=null) {
                    ByteArrayInputStream inputStream = new ByteArrayInputStream(byteArray2);
                    Bitmap bm = BitmapFactory.decodeStream(inputStream);
                    imageView.setImageBitmap(bm);
                }

                break;

        }
    }
}
