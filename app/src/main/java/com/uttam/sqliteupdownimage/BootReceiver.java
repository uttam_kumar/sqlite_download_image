package com.uttam.sqliteupdownimage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
//        // install call
//        if (intent.getAction().equals("android.intent.action.PACKAGE_ADDED")) {
//            //code here on install
//            Log.d("Installed:", "appppppppppps: "+intent.getDataString());
//        }

        // uninstall call
        if (intent.getAction().equals("android.intent.action.PACKAGE_REMOVED")) {
            String packageName = intent.getDataString ();
            Log.d ("uninstall:" , packageName + "package name of the program");
        }
    }
}